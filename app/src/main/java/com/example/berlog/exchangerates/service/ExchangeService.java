package com.example.berlog.exchangerates.service;

import com.example.berlog.exchangerates.service.pojo.DailyExRates;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by berlog on 8/17/18.
 */

public interface ExchangeService {

    @GET("/Services/XmlExRates.aspx")
    Call<DailyExRates> getRates();
}
