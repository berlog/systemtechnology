package com.example.berlog.exchangerates.view.fragment;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.berlog.exchangerates.R;
import com.example.berlog.exchangerates.service.pojo.Currency;
import com.example.berlog.exchangerates.view.adapter.ItemAdapter;
import com.woxthebox.draglistview.BoardView;

import java.util.ArrayList;

/**
 * Created by berlog on 8/17/18.
 */

public class BoardFragment extends Fragment {

    private BoardView mBoardView;

    ArrayList<Pair<Integer, Currency>> mItemArray = new ArrayList<>();
    SwipeRefreshLayout mRefreshLayout;

    public static BoardFragment newInstance(ArrayList<Pair<Integer, Currency>> mItemArray, SwipeRefreshLayout mRefreshLayout) {
        return new BoardFragment(mItemArray, mRefreshLayout);
    }

    public BoardFragment() {
    }

    @SuppressLint("ValidFragment")
    public BoardFragment(ArrayList<Pair<Integer, Currency>> mItemArray, SwipeRefreshLayout mRefreshLayout) {
        this.mItemArray = mItemArray;
        this.mRefreshLayout = mRefreshLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.board_layout, container, false);

        mBoardView = view.findViewById(R.id.board_view);
        mBoardView.setSnapToColumnsWhenScrolling(true);
        mBoardView.setSnapToColumnWhenDragging(true);
        mBoardView.setSnapDragItemToTouch(true);
        mBoardView.setSnapToColumnInLandscape(false);
        mBoardView.setColumnSnapPosition(BoardView.ColumnSnapPosition.CENTER);
        Resources res = getResources();
        mBoardView.setColumnWidth(res.getDisplayMetrics().widthPixels);

        mBoardView.setBoardListener(new BoardView.BoardListener() {
            @Override
            public void onItemDragStarted(int column, int row) {
                mRefreshLayout.setEnabled(false);
            }

            @Override
            public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {
                mRefreshLayout.setEnabled(true);
                if (fromColumn != toColumn || fromRow != toRow) {
                }
            }

            @Override
            public void onItemChangedPosition(int oldColumn, int oldRow, int newColumn, int newRow) {

            }

            @Override
            public void onItemChangedColumn(int oldColumn, int newColumn) {

            }

            @Override
            public void onFocusedColumnChanged(int oldColumn, int newColumn) {

            }

            @Override
            public void onColumnDragStarted(int position) {

            }

            @Override
            public void onColumnDragChangedPosition(int oldPosition, int newPosition) {

            }

            @Override
            public void onColumnDragEnded(int position) {

            }
        });

        addColumn();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void addColumn() {
        final ItemAdapter listAdapter = new ItemAdapter(mItemArray, R.layout.column_item, R.id.item_layout, true);
        mBoardView.addColumn(listAdapter, null, null, false);
    }
}
