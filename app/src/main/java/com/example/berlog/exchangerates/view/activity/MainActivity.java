package com.example.berlog.exchangerates.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.berlog.exchangerates.R;
import com.example.berlog.exchangerates.model.resource.ExchangeResource;
import com.example.berlog.exchangerates.service.pojo.Currency;
import com.example.berlog.exchangerates.service.pojo.DailyExRates;
import com.example.berlog.exchangerates.view.fragment.BoardFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        mRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getExchangeRatesTask();
            }
        });

        getExchangeRatesTask();
    }

    Call<DailyExRates> call;

    private void getExchangeRatesTask() {
        if (hasConnection(getApplicationContext())) {
            call = new ExchangeResource().getRates();
            call.enqueue(new Callback<DailyExRates>() {
                @Override
                public void onResponse(@NonNull Call<DailyExRates> call, @NonNull Response<DailyExRates> response) {
                    if (response.isSuccessful()) {
                        DailyExRates dailyExRates = response.body();

                        List<Currency> currencyList = null;
                        if (dailyExRates != null) {
                            currencyList = dailyExRates.getCurrency();

                            final ArrayList<Pair<Integer, Currency>> mItemArray = new ArrayList<>();
                            for (int i = 0; i < currencyList.size(); i++) {
                                mItemArray.add(new Pair<>(i, currencyList.get(i)));
                            }
                            showFragment(BoardFragment.newInstance(mItemArray, mRefreshLayout));
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),
                                response.message(), Toast.LENGTH_LONG).show();
                    }
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(@NonNull Call<DailyExRates> call, @NonNull Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(getApplicationContext(),
                                t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                    mRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            String title = getResources().getString(R.string.title_alert_dialog_internet_connection);
            String message = getResources().getString(R.string.txt_alert_dialog_internet_connection);
            getAlertConnectionDialog(title, message);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (call != null) {
            call.cancel();
        }
    }

    private void getAlertConnectionDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.txt_alert_dialog_start_positive_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getExchangeRatesTask();
                    }
                })
                .setNegativeButton(R.string.txt_alert_dialog_start_negative_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, "fragment").commit();
    }
}
