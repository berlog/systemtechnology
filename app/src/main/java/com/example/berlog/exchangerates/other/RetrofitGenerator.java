package com.example.berlog.exchangerates.other;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class RetrofitGenerator {

    private Retrofit retrofit;

    public RetrofitGenerator() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Const.URL_SCHEME.concat(Const.HOST).concat(":").concat(Const.PORT))
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}