package com.example.berlog.exchangerates.model.resource;

import com.example.berlog.exchangerates.other.RetrofitGenerator;
import com.example.berlog.exchangerates.service.ExchangeService;
import com.example.berlog.exchangerates.service.pojo.DailyExRates;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by berlog on 8/17/18.
 */

public class ExchangeResource {

    private ExchangeService service;

    public ExchangeResource() {
        Retrofit retrofit = new RetrofitGenerator().getRetrofit();
        service = retrofit.create(ExchangeService.class);
    }

    //GET
    public Call<DailyExRates> getRates() {
        return service.getRates();
    }
}
