package com.example.berlog.exchangerates.service.pojo;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by berlog on 8/17/18.
 */

@Root(name = "Currency")
public class Currency {

    @Attribute(name = "Id", required = false)
    private String id;

    @Element(name = "NumCode")
    private String numCode;

    @Element(name = "CharCode")
    private String charCode;

    @Element(name = "Scale")
    private int scale;

    @Element(name = "Name")
    private String name;

    @Element(name = "Rate")
    private float rate;

    public Currency() {

    }

    public String getId() {
        return id;
    }

    public String getNumCode() {
        return numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    public int getScale() {
        return scale;
    }

    public String getName() {
        return name;
    }

    public float getRate() {
        return rate;
    }
}
