package com.example.berlog.exchangerates.service.pojo;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by berlog on 8/17/18.
 */

@Root(name = "DailyExRates")
public class DailyExRates {

    @Attribute(name = "Date")
    private String date;

    @ElementList(name = "Currency", inline = true)
    private List<Currency> currency;

    public DailyExRates() {

    }

    public String getDate() {
        return date;
    }

    public List<Currency> getCurrency() {
        return currency;
    }
}
