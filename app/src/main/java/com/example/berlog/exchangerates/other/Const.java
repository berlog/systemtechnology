package com.example.berlog.exchangerates.other;

/**
 * Created by berlog on 8/17/18.
 */

public interface Const {
    String URL_SCHEME = "http://";
    String HOST = "www.nbrb.by";
    String PORT = "80";
}
