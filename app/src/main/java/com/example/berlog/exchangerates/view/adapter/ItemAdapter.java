package com.example.berlog.exchangerates.view.adapter;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.berlog.exchangerates.R;
import com.example.berlog.exchangerates.service.pojo.Currency;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

public class ItemAdapter extends DragItemAdapter<Pair<Integer, Currency>, ItemAdapter.ViewHolder> {

    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;

    public ItemAdapter(ArrayList<Pair<Integer, Currency>> list, int layoutId, int grabHandleId, boolean dragOnLongPress) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        setItemList(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Currency currency = mItemList.get(position).second;
        if (currency != null) {
            holder.tvCharCode.setText(currency.getCharCode());
            holder.tvRate.setText(String.valueOf(currency.getRate()));
            holder.tvName.setText(currency.getName());
            holder.tvScale.setText(String.valueOf(currency.getScale()));
        }
        holder.itemView.setTag(mItemList.get(position));
    }

    @Override
    public long getUniqueItemId(int position) {
        return mItemList.get(position).first;
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {
        TextView tvCharCode;
        TextView tvRate;
        TextView tvName;
        TextView tvScale;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            tvCharCode = (TextView) itemView.findViewById(R.id.tv_char_code);
            tvRate = (TextView) itemView.findViewById(R.id.tv_rate);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvScale = (TextView) itemView.findViewById(R.id.tv_scale);
        }

        @Override
        public void onItemClicked(View view) {
        }

        @Override
        public boolean onItemLongClicked(View view) {
            return true;
        }
    }
}
